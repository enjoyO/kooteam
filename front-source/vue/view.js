import View from "./view/index"
import Preview from "./view/preview"

Vue.component("k-view", View);
Vue.component("k-preview", Preview);